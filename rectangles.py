def rectangle(x,y,w,h):
    return x,y,w,h

def getx(r):
    return r[0]

def gety(r):
    return r[1]

def getw(r):
    return r[2]

def geth(r):
    return r[3]

def get_center(r):
    return getx(r) + getw(r)/2 , gety(r) -geth(r)/2

def get_surface(r):
    return getw(r) * geth(r)
