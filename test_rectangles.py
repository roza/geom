from rectangles import *

def test_creation_rect():
    rect = rectangle(200,100, 400, 300)
    assert getx(rect) == 200
    assert gety(rect) == 100
    assert getw(rect) == 400
    assert geth(rect) == 300


def test_center():
    rect = rectangle(200,600, 400, 300)
    print(get_center(rect))
    assert get_center(rect) == (400 , 450)


def test_surface():
    rect = rectangle(200,600, 400, 300)
    assert get_surface(rect) == 120000
